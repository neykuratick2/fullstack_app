import hashlib

from django.db import models


# Create your models here.

def upload_to(instance, filename):
    return f"keyboards/{hashlib.md5(instance.title.encode('utf-8')).hexdigest()}.png"


class Keyboard(models.Model):
    title = models.CharField(max_length=300)
    picture = models.ImageField(null=True, blank=True, upload_to=upload_to)
    price = models.IntegerField()
    switches = models.CharField(max_length=300)
    keycaps = models.CharField(max_length=300)
    noise_isolation = models.BooleanField()
    format = models.CharField(max_length=300)
    owner = models.CharField(max_length=300)
