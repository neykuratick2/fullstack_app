from django.urls import path
from api.views import GetKeyboards, UploadKeyboard, DeleteKeyboard

urlpatterns = [
    path('get/', GetKeyboards.as_view()),
    path('create/', UploadKeyboard.as_view()),
    path('delete/', DeleteKeyboard.as_view()),
]
