import hashlib

from django.contrib.auth import login
from rest_framework import status, generics, permissions
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from knox.models import AuthToken

from api.serializers import KeyboardSerializer, RegisterSerializer, UserSerializer
from base.models import Keyboard
from knox.views import LoginView as KnoxLoginView


class GetKeyboards(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, format=None):
        print(request)
        keyboards = Keyboard.objects.filter(owner=request.data.get("owner"))
        serializer = KeyboardSerializer(keyboards, many=True)
        return Response(serializer.data)


class DeleteKeyboard(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, format=None):
        keyboard = Keyboard.objects.get(id=request.data.get("id"))
        keyboard.delete()
        return Response("deleted")


class UploadKeyboard(APIView):
    parser_classes = [MultiPartParser, FormParser]
    authentication_classes = []
    permission_classes = []

    def post(self, request, format=None):
        serializer = KeyboardSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Register API
class RegisterAPI(generics.GenericAPIView):
    serializer_class = RegisterSerializer
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response({
            # "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })


class LoginAPI(KnoxLoginView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = AuthTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        return super(LoginAPI, self).post(request, format=None)
