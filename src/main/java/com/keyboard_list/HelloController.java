package com.keyboard_list;

import com.keyboard_list.backend.Api;
import com.keyboard_list.backend.ContextManager;
import com.keyboard_list.models.ServerResponse;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.io.IOException;

public class HelloController {
    @FXML private Label welcomeText;
    @FXML private TextField field_username;
    @FXML private TextField field_password;

    public void switch_to_main_scene(ActionEvent event) throws IOException {
        Utils utils = new Utils();
        utils.load_scene("main-view.fxml", event);
    }


    @FXML
    protected void onHelloButtonClick(ActionEvent event) throws Exception {
        Api server = new Api();
        try {
            ServerResponse<String> server_response = server.login(field_username.getText(), field_password.getText());
            ContextManager.set_token(server_response.payload);
            switch_to_main_scene(event);
        } catch (Exception exception) {
            welcomeText.setText("Wrong username or password");;
        }
    }

    @FXML
    protected void register(ActionEvent event) throws Exception {
        Api server = new Api();
        try {
            ServerResponse<String> server_response = server.register(field_username.getText(), field_password.getText());
            ContextManager.set_token(server_response.payload);
            ContextManager.set_username(field_username.getText());
            switch_to_main_scene(event);
        } catch (Exception exception) {
            welcomeText.setText("Something went wrong");;
        }
    }
}