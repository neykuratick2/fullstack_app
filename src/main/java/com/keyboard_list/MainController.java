package com.keyboard_list;

import com.keyboard_list.backend.ContextManager;
import com.keyboard_list.models.Defaults;
import com.keyboard_list.models.KeyboardModel;
import com.keyboard_list.backend.Api;
import com.keyboard_list.models.ServerResponse;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.util.ArrayList;

public class MainController {
    @FXML VBox keyboardVboxList;
    @FXML Label state_label;
    @FXML ImageView keyboard_pic;
    @FXML TextField search_box;
    @FXML Label field_title;
    @FXML Label field_price;
    @FXML Label field_switches;
    @FXML Label field_keycaps;
    @FXML Label field_noise;
    @FXML Label field_format;
    @FXML VBox keyboard_view;

    @FXML
    protected void initialize() throws Exception {
        update_keyboard_view();
    }

    public void reset_image(String image_url) {
        Image default_image = new Image(image_url);
        keyboard_pic.setImage(default_image);
    }

    public void update_keyboard_view() throws Exception {
        reset_image(Defaults.none_picture);

        Api server = new Api();
        ServerResponse<ArrayList<KeyboardModel>> response = server.get_keyboards();
        ArrayList<KeyboardModel> keyboards = response.payload;

        keyboardVboxList.getChildren().clear();

        for (KeyboardModel keyboard_model: keyboards) {
            HBox hbox = new HBox();
            hbox.setAlignment(Pos.CENTER);

            Label label = new Label(keyboard_model.Title);
            Button button = new Button("View");
            button.setOnAction((event) -> {
                get_keyboard(keyboard_model);
            });

            HBox.setMargin(label, new Insets(0, 10, 0, 10));

            hbox.getChildren().add(label);
            hbox.getChildren().add(button);

            keyboardVboxList.getChildren().add(hbox);
        }
    }

    public void delete_keyboard(KeyboardModel keyboardModel, ActionEvent event) {

        try {
            Api api = new Api();
            api.delete_keyboard(keyboardModel.Id);

            Utils utils = new Utils();
            utils.load_scene("main-view.fxml", event);
        } catch (Exception exception) {
            state_label.setText("App crashed");
        }
//        update_keyboard_view();
    }

    public void get_keyboard(KeyboardModel keyboard) {
        state_label.setText("Loading image, please wait");

        System.out.println(keyboard.Id);

        String noise;
        if (keyboard.NoiseIsolation) {
            noise = "Yes";
        } else {
            noise = "Absent";
        }

        System.out.println(keyboard.Pic_url);
        field_title.setText(keyboard.Title);
        field_price.setText("" + keyboard.Price);
        field_switches.setText(keyboard.Switches);
        field_keycaps.setText(keyboard.Keycaps);
        field_noise.setText(noise);
        field_format.setText(keyboard.Format);

        Button button = new Button("Delete");
        button.setOnAction((event) -> {
            delete_keyboard(keyboard, event);
        });

        keyboard_view.getChildren().clear();
        keyboard_view.getChildren().add(button);

        reset_image(keyboard.Pic_url);
        state_label.setText(ContextManager.get_username());
    }

    public void add_keyboard(ActionEvent event) throws IOException {
        Utils utils = new Utils();
        utils.load_scene("add-keyboard.fxml", event);
//        ContextManager.set_title(search_box.getText());
//        Api api = new Api();
//        ServerResponse<String> response = api.add_keyboard();
//        state_label.setText("updated");
//        update_keyboard_view();
    }

    public void log_out(ActionEvent event) throws IOException {
        Utils utils = new Utils();
        utils.load_scene("hello-view.fxml", event);
    }
}

