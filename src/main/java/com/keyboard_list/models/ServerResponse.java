package com.keyboard_list.models;

public class ServerResponse<T> {
    private Boolean InvalidCredentials = false;
    private Boolean InternalServerError = false;
    private Boolean Success = false;
    public T payload;

    public Boolean getSuccess() {
        return Success;
    }

    public void setSuccess() {
        Success = true;
    }

    public void setInvalidCredentials() {
        InvalidCredentials = true;
    }

    public void setInternalServerError() {
        InternalServerError = true;
    }

}
