package com.keyboard_list.models;

public class KeyboardModel {
    public String Title;
    public String Pic_url;
    public int Price;
    public String Switches;
    public String Keycaps;
    public Boolean NoiseIsolation;
    public String Format;
    public int Id;

    public KeyboardModel(
            String title,
            String pic_url,
            int price,
            String switches,
            String keycaps,
            Boolean noiseIsolation,
            String format,
            int id
            ) {
        Title = title;
        Pic_url = pic_url;
        Price = price;
        Switches = switches;
        Keycaps = keycaps;
        NoiseIsolation = noiseIsolation;
        Format = format;
        Id = id;
    }
}
