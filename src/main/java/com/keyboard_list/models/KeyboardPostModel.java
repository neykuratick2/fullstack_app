package com.keyboard_list.models;

import java.io.File;

public class KeyboardPostModel {
    public String Title;
    public int Price;
    public String Switches;
    public String Keycaps;
    public Boolean NoiseIsolation;
    public String Format;
    public File Picture;
    public String Owner;

    public KeyboardPostModel(
            String title,
            int price,
            String switches,
            String keycaps,
            Boolean noiseIsolation,
            String format,
            File picture,
            String owner
    ) {
        Title = title;
        Price = price;
        Switches = switches;
        Keycaps = keycaps;
        NoiseIsolation = noiseIsolation;
        Format = format;
        Picture = picture;
        Owner = owner;
    }
}
