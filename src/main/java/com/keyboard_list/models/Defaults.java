package com.keyboard_list.models;

import com.keyboard_list.models.KeyboardModel;

public class Defaults {
//    public static String none_picture = "https://icons.iconarchive.com/icons/custom-icon-design/pretty-office-2/256/man-icon.png";
    public static String none_picture = "https://image.shutterstock.com/image-vector/keyboard-illustration-icon-logo-vector-260nw-1700183038.jpg";
    public static String keyboard_url = "https://image.shutterstock.com/image-vector/keyboard-illustration-icon-logo-vector-260nw-1700183038.jpg";
    public static String base_server = "http://mybigdick.xyz:8000";
    public static String post_keyboard_url = base_server + "/keyboards/create/";
    public static String delete_url = base_server + "/keyboards/delete/";
    public static String get_keyboards_url = base_server + "/keyboards/get/?format=json";
    public static String login_url = base_server + "/api/login/";
    public static String register_url = base_server + "/api/register/?format=json";
    public static KeyboardModel keyboardModel = new KeyboardModel(
            "Offline keyboard",
            keyboard_url,
            228,
            "Cherry MX red",
            "ABS plastic",
            true,
            "80%",
            1
    );
    public String token;
}
