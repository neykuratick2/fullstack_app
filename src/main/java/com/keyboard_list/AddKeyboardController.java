package com.keyboard_list;

import com.keyboard_list.backend.ContextManager;
import com.keyboard_list.models.KeyboardPostModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static com.keyboard_list.Utils.isInteger;
import static com.keyboard_list.backend.Api.upload_keyboard;

public class AddKeyboardController {
    @FXML HBox noise_handler;
    @FXML Button field_picture;
    @FXML ChoiceBox<Boolean> field_noise;
    @FXML TextField field_title;
    @FXML TextField field_price;
    @FXML TextField field_switches;
    @FXML TextField field_keycaps;
    @FXML TextField field_format;
    @FXML VBox price_error_box;
    @FXML Label message_box;
    private KeyboardPostModel keyboard;

    @FXML
    protected void initialize() {
//        field_title.setText(ContextManager.get_title());
        ArrayList<Boolean> list = new ArrayList<>();
        list.add(true);
        list.add(false);

        ObservableList<Boolean> observableList = FXCollections.observableList(list);
        field_noise.setItems(observableList);
    }

    @FXML
    public void choose_picture(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png");
        fileChooser.getExtensionFilters().add(extFilter);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        File file = fileChooser.showOpenDialog(stage);
        field_picture.setText(file.getName());
        field_picture.setUserData(file);
    }

    @FXML
    public void go_back(ActionEvent event) throws IOException {
        Utils utils = new Utils();
        utils.load_scene("main-view.fxml", event);
    }

    @FXML
    public void submit(ActionEvent event) throws Exception {
        Utils utils = new Utils();

        if (!isInteger(field_price.getText())) {
            price_error_box.getChildren().add(new Label("Enter a valid integer"));
            return;
        }

        File picture = (File) field_picture.getUserData();
        Boolean noise = field_noise.getValue();
        String title = field_title.getText();
        int price = Integer.parseInt(field_price.getText());
        String switches = field_switches.getText();
        String keycaps = field_keycaps.getText();
        String format = field_format.getText();
        System.out.println(picture);

        KeyboardPostModel keyboard = new KeyboardPostModel(
                title,
                price,
                switches,
                keycaps,
                noise,
                format,
                picture,
                ContextManager.get_token()
        );

        try {
            upload_keyboard(keyboard);

        } catch (Exception exception) {
            message_box.setText(exception.getMessage());
            return;
        }


        utils.load_scene("main-view.fxml", event);

    }
}
