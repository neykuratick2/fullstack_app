package com.keyboard_list.backend;

public class ContextManager {
    private static final ThreadLocal<String> token = new ThreadLocal<>();
    private static final ThreadLocal<String> username = new ThreadLocal<>();

    public static void set_username(String auth) {
        token.set(auth);
    }

    public static String get_username() {
        return token.get();
    }

    public static void set_token(String auth) {
        token.set(auth);
    }

    public static String get_token() {
        return token.get();
    }

    public static void clear(){
        token.remove();
        username.remove();
    }
}