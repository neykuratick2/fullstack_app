package com.keyboard_list.backend;

import com.keyboard_list.models.Defaults;
import com.keyboard_list.models.KeyboardModel;
import com.keyboard_list.models.KeyboardPostModel;
import com.keyboard_list.models.ServerResponse;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.keyboard_list.Utils.*;
import static com.keyboard_list.models.Defaults.*;
import static java.lang.Integer.parseInt;

public class Api {
    public ServerResponse<String> login(String username, String password) throws Exception {
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("username", username);
        params.put("password", password);

        JSONObject response_json = new JSONObject(make_post_request(login_url, params));

        ServerResponse<String> response = new ServerResponse<String>();
        response.setSuccess();
        response.payload = response_json.get("token").toString();
        return response;
    }

    public ServerResponse<String> register(String username, String password) throws Exception {
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("username", username);
        params.put("password", password);

        String response_string = make_post_request(register_url, params);
        System.out.println(response_string);
        JSONObject response_json = new JSONObject(response_string);

        ServerResponse<String> response = new ServerResponse<String>();
        response.setSuccess();
        response.payload = response_json.get("token").toString();
        return response;
    }

    public ServerResponse<ArrayList<KeyboardModel>> get_keyboards() throws Exception {

        Map<String,Object> params = new LinkedHashMap<>();
        params.put("owner", ContextManager.get_token());

        ArrayList<JSONObject> keyboards_json_array;

        try {
            String response_string = make_post_request(get_keyboards_url, params);
            keyboards_json_array = new ArrayList<>();
            JSONArray response_array = new JSONArray(response_string);

            for (Object response_raw : response_array) {
                keyboards_json_array.add(new JSONObject(response_raw.toString()));
            }

        } catch (ConnectException exception) {
            ServerResponse<ArrayList<KeyboardModel>> response = new ServerResponse<ArrayList<KeyboardModel>> ();
            response.setInternalServerError();
            response.payload = new ArrayList<>();
            response.payload.add(Defaults.keyboardModel);
            return response;
        }

        ServerResponse<ArrayList<KeyboardModel>> response = new ServerResponse<ArrayList<KeyboardModel>> ();
        response.setSuccess();

        if (response.getSuccess()) {
            ArrayList<KeyboardModel> array = new ArrayList<>();

            for (JSONObject keyboard_json: keyboards_json_array) {
                boolean noise;
                int price;
                String pic_url;

                if (keyboard_json.get("picture").toString().equals("null")) {
                    pic_url = Defaults.keyboard_url;
                } else {
                    pic_url = base_server + keyboard_json.get("picture").toString();
                }

                if (!isInteger(keyboard_json.get("price").toString())) {
                    price = 0;
                }  else {
                    price = parseInt(keyboard_json.get("price").toString());
                }

                noise = Boolean.parseBoolean(keyboard_json.get("noise_isolation").toString());

                array.add(new KeyboardModel(
                        keyboard_json.get("title").toString(),
                        pic_url,
                        price,
                        keyboard_json.get("switches").toString(),
                        keyboard_json.get("keycaps").toString(),
                        noise,
                        keyboard_json.get("format").toString(),
                        Integer.parseInt(keyboard_json.get("id").toString())
                ));
            }

            response.payload = array;
        } else {
            response.payload = new ArrayList<KeyboardModel>();
        }

        return response;
    }

    public void delete_keyboard(int keyboard_id) throws Exception {

        Map<String,Object> params = new LinkedHashMap<>();
        params.put("id", keyboard_id);

        make_post_request(delete_url, params);

        ServerResponse<String> response = new ServerResponse<String>();
        response.setSuccess();
    }

    public static void upload_keyboard(KeyboardPostModel keyboard) throws Exception {
        String noise;
        if (keyboard.NoiseIsolation) {
            noise = "True";
        } else {
            noise = "False";
        }

        MultipartUtility multipart = new MultipartUtility(post_keyboard_url, "UTF-8");
        multipart.addFormField("title", keyboard.Title);
        multipart.addFormField("price", "" + keyboard.Price);
        multipart.addFormField("keycaps", keyboard.Keycaps);
        multipart.addFormField("noise_isolation", noise);
        multipart.addFormField("format", keyboard.Title);
        multipart.addFormField("switches", keyboard.Switches);
        multipart.addFormField("owner", keyboard.Owner);
        multipart.addFilePart("picture", keyboard.Picture);
        multipart.addHeaderField("Authorization", ContextManager.get_token());
        String response = multipart.finish(); // response from server.
    }
}
