module com.keyboard_list {
    requires javafx.controls;
    requires javafx.fxml;
        requires javafx.web;
            
        requires org.controlsfx.controls;
            requires com.dlsc.formsfx;
            requires validatorfx;
            requires org.kordamp.ikonli.javafx;
            requires org.kordamp.bootstrapfx.core;
            requires eu.hansolo.tilesfx;
    requires org.json;

    opens com.keyboard_list to javafx.fxml;
    exports com.keyboard_list;
    exports com.keyboard_list.backend;
    exports com.keyboard_list.models;
    opens com.keyboard_list.models to javafx.fxml;
}